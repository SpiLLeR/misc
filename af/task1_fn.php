<?php
$string = '[{"_id":"4940dgfd95jfsd9","score":15,"user":0,"type":"xxx"},
{"_id":"4940dsdsvhjttt89","score":4,"user":0,"type":"yyy"},
{"_id":"4941dgfdsdfggd9","score":7,"user":0,"type":"yyy"},
{"_id":"4941dfds43rfsff9","score":21,"user":1,"type":"yyy"},
{"_id":"4941dgfd93jfsd0","score":15,"user":1,"type":"xxx"},
{"_id":"4942dgdsadvfd9","score":8,"user":1,"type":"yyy"},
{"_id":"4942dgfdaefcod8","score":4,"user":2,"type":"yyy"},
{"_id":"4942dgfdsarfsd1","score":32,"user":2,"type":"yyy"},
{"_id":"4943dgfddasjfsd5","score":20,"user":2,"type":"xxx"},
{"_id":"4943dasrtqxrfsd9","score":15,"user":3,"type":"xxx"},
{"_id":"4943dgfooorafsd8","score":15,"user":3,"type":"yyy"},
{"_id":"4944dgfddadfsd3","score":15,"user":3,"type":"yyy"},
{"_id":"4944dgfd9dssdc4","score":15,"user":4,"type":"xxx"},
{"_id":"4944dgdacbbrttt9","score":19,"user":4,"type":"yyy"},
{"_id":"4945dgfsaddddd5","score":15,"user":4,"type":"xxx"}]';

function removeLowScoredYYY($string) {
    $excludeUsers = [];
    $rawRows = explode("\n", $string);
    unset($strin);
    $output = '';
    foreach($rawRows as $row) {
        preg_match('~(\[{0,1})?({.*})(\]{0,1})?~', $row, $matches);
        list(, $firstChar, $json, $lastChar) = $matches;

        $user = json_decode($json);
        if($user->type == 'yyy') {
            if(!isset($excludeUsers[$user->user])) {
                $excludeUsers[$user->user] = [
                    'data' => $user,
                    'counter' => 1,
                ];
                continue;
            } else if($user->score == $excludeUsers[$user->user]['data']->score) {
                $tmp = $excludeUsers[$user->user];
                $excludeUsers[$user->user] = [
                    'data' => $user,
                    'counter' => $tmp['counter'],
                ];
                $user = $tmp['data'];
            } else {
                $excludeUsers[$user->user]['counter']++;
                if($user->score < $excludeUsers[$user->user]['data']->score) {
                    $tmp = $excludeUsers[$user->user];
                    $excludeUsers[$user->user] = [
                        'data'    => $user,
                        'counter' => $tmp['counter'],
                    ];
                    $user = $tmp['data'];
                }
            }

        }

        if(!empty($firstChar)) {
            $output .= '[';
        }

        $output .= json_encode($user);

        if(!empty($lastChar)) {
            foreach($excludeUsers as $key => $item) {
                if($item['counter'] !== 1) {
                    continue;
                }

                $output .= ',' . "\n" . json_encode($item['data']);
            }
            $output .= ']';
        } else {
            $output .= ',';
        }

        $output .= "\n";
    }

    return $output;
}

echo removeLowScoredYYY($string);
