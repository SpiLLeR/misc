<?php
$string = '[
  {
    "_id":1364,"name":"Predator","occupations":[
      {"title":"exploring","time":54.9,"activityLevel":1},
      {"title":"hunting","time":22.4,"activityLevel":2},
      {"title":"sleep","time":5.1,"activityLevel":0},
      {"title":"nap","time":2.0,"activityLevel":0},
      {"title":"slacking","time":10.0,"activityLevel":0},
      {"title":"cooking","time":9.0,"activityLevel":1}
    ]
  },
  {
    "_id":1365,"name":"Alien","occupations":[
      {"title":"sleep","time":5.1,"activityLevel":0},
      {"title":"nap","time":2.0,"activityLevel":0},
      {"title":"exploring","time":32.1,"activityLevel":1},
      {"title":"slacking","time":20.1,"activityLevel":0},
      {"title":"cooking","time":0,"activityLevel":1},
      {"title":"hunting","time":43.2,"activityLevel":2}
    ]
  },
  {
    "_id":1366,"name":"Harry Potter","occupations":[
      {"title":"sleep","time":30.0,"activityLevel":0},
      {"title":"exploring","time":10.5,"activityLevel":1},
      {"title":"slacking","time":30.1,"activityLevel":0},
      {"title":"cooking","time":0,"activityLevel":1},
      {"title":"dreaming","time":10.0,"activityLevel":0},
      {"title":"nap","time":10.0,"activityLevel":0},
      {"title":"hunting","time":0,"activityLevel":2}
    ]
  }
]';

function removeLowActivity($string)
{
    $catching     = false;
    $buffer       = '';
    $rawData = explode("\n", $string);
    unset($string);
    $output = '';
    foreach ($rawData as $row) {
        $line = '';
        if (strpos($row, 'occupations') !== false) {
            $catching = true;
            $line .= $row . "\n";
        } else if (strpos($row, ']') !== false) {
            $catching                  = false;
            $data                      = json_decode('[' . $buffer . ']');
            $excludeRow                = new \stdClass();
            $excludeRow->time          = 100.00;
            $excludeRow->activityLevel = 100;
            $excludeKey                = null;
            foreach ($data as $i => $item) {
                if ($item->time < $excludeRow->time && $item->activityLevel <= 1) {
                    $excludeRow = $item;
                    $excludeKey = $i;
                }
            }

            unset($data[$excludeKey]);
            foreach ($data as $item) {
                $line .= "\t\t" . json_encode($item) . ", \n";
            }

            if (!empty($data)) {
                $line[strrpos($line, ',')] = '';
                $line .= "\t]\n";
            }

            $buffer = '';
        }

        if ($catching && empty($line)) {
            $buffer .= $row . "\n";
        }

        if (!$catching && empty($line)) {
            $line .= $row . "\n";
        }

        $output .= $line;
    }

    return $output;
}

echo removeLowActivity($string);

