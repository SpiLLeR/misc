<?php

$handle = fopen("php://stdin", "r");
$excludeUsers = [];
$rowNumber = 0;
if ($handle) {
    while (($row = fgets($handle, 4096)) !== false) {
        $output = '';
        preg_match('~(\[{0,1})?({.*})(\]{0,1})?~', $row, $matches);
        list(, $firstChar, $json, $lastChar) = $matches;

        $user = json_decode($json);
        if($user->type == 'yyy') {
            if(!isset($excludeUsers[$user->user])) {
                $excludeUsers[$user->user] = [
                    'data' => $user,
                    'counter' => 1,
                ];
                continue;
            } else if($user->score == $excludeUsers[$user->user]['data']->score) {
                $tmp = $excludeUsers[$user->user];
                $excludeUsers[$user->user] = [
                    'data' => $user,
                    'counter' => $tmp['counter'],
                ];
                $user = $tmp['data'];
            } else {
                $excludeUsers[$user->user]['counter']++;
                if($user->score < $excludeUsers[$user->user]['data']->score) {
                    $tmp = $excludeUsers[$user->user];
                    $excludeUsers[$user->user] = [
                        'data'    => $user,
                        'counter' => $tmp['counter'],
                    ];
                    $user = $tmp['data'];
                }
            }

        }

        if(!empty($firstChar)) {
            $output .= '[';
        }

        $output .= json_encode($user);

        if(!empty($lastChar)) {
            foreach($excludeUsers as $key => $item) {
                if($item['counter'] !== 1) {
                    continue;
                }

                $output .= ',' . "\n" . json_encode($item['data']);
            }
            $output .= ']';
        } else {
            $output .= ',';
        }

        echo $output . "\n";
    }

    fclose($handle);
}
