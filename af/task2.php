<?php

$handle = fopen("php://stdin", "r");
$excludeUsers = [];
$rowNumber = 0;
$catching = false;
$buffer = '';
if ($handle) {
    while (($row = fgets($handle, 4096)) !== false) {
        $output = '';
        if(strpos($row, 'occupations') !== false) {
            $catching = true;
            $output .= $row;
        } else if(strpos($row, ']') !== false) {
            $catching = false;
            $data = json_decode('[' . $buffer . ']');
            $excludeRow = new \stdClass();
            $excludeRow->time = 100.00;
            $excludeRow->activityLevel = 100;
            $excludeKey = null;
            foreach($data as $i => $item) {
                if($item->time < $excludeRow->time && $item->activityLevel <= 1) {
                    $excludeRow = $item;
                    $excludeKey = $i;
                }
            }

            unset($data[$excludeKey]);
            foreach($data as $item) {
                $output .= "\t\t" . json_encode($item) . ", \n";
            }

            if(!empty($data)) {
                $output[strrpos($output, ',')] = '';
                $output .= "\t]\n";
            }

            $buffer = '';
        }

        if($catching && empty($output)) {
            $buffer .= $row;
        }

        if(!$catching && empty($output)) {
            $output .= $row;
        }

        echo $output;
    }

    fclose($handle);
}
