<?php
$filename = isset($argv[1]) ? $argv[1] : '';
if(empty($filename)) {
    throw new \Exception('Please set input filename with analyze string.');
}

if(!is_readable($filename)) {
    throw new \Exception('File ' . $filename . ' is not readable.');
}

$input = file_get_contents($filename);
$inputWords = explode(' ', $input);
$count      = 0;
$limit      = 20;

while (count($inputWords)) {
    $chunk   = array_slice($inputWords, 0, $limit);
    $streams = generateStreams($chunk);
    $count += run($streams);
    $inputWords = array_slice($inputWords, $limit);
}

echo $count . "\n";

/**
 * @param string $inputWords
 *
 * @return array
 */
function generateStreams($inputWords)
{
    $streams = [];
    foreach ($inputWords as $inputWord) {
        $cmd       = sprintf('php ./handler.php %s', $inputWord);
        $streams[] = popen($cmd, 'r');
    }

    return $streams;
}

/**
 * @param array $streams
 *
 * @return int
 */
function run(array $streams)
{
    $timeout = 10;
    $count   = 0;
    while (count($streams)) {
        $read = $streams;
        stream_select($read, $w = null, $e = null, $timeout);
        foreach ($read as $r) {
            $id = array_search($r, $streams);
            $count += (int)stream_get_contents($streams[$id]);
            if (feof($r)) {
                pclose($streams[$id]);
                unset($streams[$id]);
            }
        }
    }

    return $count;
}