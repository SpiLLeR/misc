<?php
$content = file_get_contents('./vocabulary.txt');
$words = explode("\n", $content);

foreach($words as $i => $word) {
    $words[$i] = strtolower($word);
}

file_put_contents('vocabulary.txt', implode("\n", $words));