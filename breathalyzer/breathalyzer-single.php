<?php
$filename = isset($argv[1]) ? $argv[1] : '';
if (empty($filename)) {
    throw new \Exception('Please set input filename with analyze string.');
}

if (!is_readable($filename)) {
    throw new \Exception('File ' . $filename . ' is not readable.');
}

$start = microtime(true);

$input      = file_get_contents($filename);
$content    = file_get_contents('./vocabulary.txt');
$words      = explode("\n", $content);
$inputWords = explode(' ', $input);

$count = parse($inputWords, $words);

echo $count . "\n";

$end = microtime(true);
printf("Execution time: %.2f \n", $end - $start);

function parse($inputWords, $words)
{
    $count       = 0;
    $storage     = [];
    $sortedWords = [];
    foreach ($words as $word) {
        $sortedWords[strlen($word)][] = $word;
    }

    foreach ($inputWords as $inputWord) {
        if (in_array($inputWord, $words)) {
            continue;
        }

        if (isset($storage[$inputWord])) {
            $count += $storage[$inputWord];
            continue;
        }

        $shortest = chunkSearch($sortedWords, $inputWord, 1);

        $storage[$inputWord] = $shortest;
        $count += $shortest;
    }

    return $count;
}

function chunkSearch($words, $inputWord, $range, $shortest = -1)
{
    if ($shortest === $range) {
        return $shortest;
    }

    $inputWordLength = strlen($inputWord);
    $firstChunk      = isset($words[$inputWordLength - $range]) ? $words[$inputWordLength - $range] : [];
    $lastChunk       = isset($words[$inputWordLength + $range]) ? $words[$inputWordLength + $range] : [];
    $currentChunk    = isset($words[$inputWordLength]) ? $words[$inputWordLength] : [];

    $result     = -1;
    $chunkWords = array_merge($firstChunk, $currentChunk, $lastChunk);
    foreach ($chunkWords as $word) {
        $lev = levenshtein($inputWord, $word);
        if ($lev <= $result || $result < 0) {
            $result = $lev;
            if ($result === $range) {
                break;
            }
        }
    }

    if ($result === $range || $result === ($range + 1)) {
        return $result;
    }

    if ($result < $shortest && $result > 0) {
        $shortest = $result;
    }

    return chunkSearch($words, $inputWord, $range + 1, $shortest);
}