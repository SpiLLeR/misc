<?php

$inputWord = $argv[1];
$content = file_get_contents('./vocabulary.txt');
$words   = explode("\n", $content);

$shortest = -1;
foreach ($words as $word) {
    $lev = levenshtein($inputWord, $word);
    if ($lev == 0) {
        $shortest = 0;
        break;
    }

    if ($lev <= $shortest || $shortest < 0) {
        $shortest = $lev;
    }
}

echo $shortest . "\n";