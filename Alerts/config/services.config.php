<?php
return [
    'aliases' => [
        'alerts.service' => 'Alerts\Service\AlertService',
    ],
    'factories' => [
        'Alerts\Service\AlertService'  => 'Alerts\Service\Factory\AlertServiceFactory',
    ],
];