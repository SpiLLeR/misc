<?php
namespace License;

return [
    'controllers' => [
        'invokables' => [
            'Alerts\Controller\Alerts' => 'Alerts\Controller\AlertsController',
        ],
    ],
    'router' => [
        'routes' => [
            'get-alerts' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '/get-alerts',
                    'defaults' => [
                        'controller' => 'Alerts\Controller\Alerts',
                        'action'     => 'get-alerts',
                    ],
                ],
            ],
            'delete-alerts' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '/delete-alerts',
                    'defaults' => [
                        'controller' => 'Alerts\Controller\Alerts',
                        'action'     => 'delete',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'alerts' => __DIR__ . '/../view',
        ],
        'strategies'               => [
            'ViewJsonStrategy'
        ]
    ],
];
