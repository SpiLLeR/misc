<?php

namespace Alerts\Controller;

use Alerts\Exception\AlertsException;
use Alerts\Model\Alert;
use Alerts\Service\AlertService;
use Alerts\Service\AlertStorage;
use APIClient\Client;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class AlertsController extends AbstractActionController
{
    /**
     * @return JsonModel
     */
    public function getAlertsAction()
    {
        /** @var AlertStorage $alerts */
        $alerts = $this->getServiceLocator()->get('alerts.service')->getAll();
        return new JsonModel($alerts->toArray());
    }

    public function deleteAction() {
        $id = $this->params()->fromPost('id', '');
        $this->getServiceLocator()->get('alerts.service')->deleteAlerts($id);
        return new JsonModel([]);
    }
}