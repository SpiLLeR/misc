<?php
namespace Alerts\Service\Factory;

use Alerts\Service\AlertService;
use Alerts\Service\AlertStorage;
use Zend\Cache\StorageFactory;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class AlertServiceFactory
 *
 * @package Alerts\Service\Factory
 */
class AlertServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return AlertService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('config');
        if(!isset($config['alerts']['storage'])) {
            throw new ServiceNotCreatedException('Config: alerts => storage is not set.');
        }

        return new AlertService(
            new AlertStorage(),
            StorageFactory::factory($config['alerts']['storage']),
            $serviceLocator->get('IdentityStorage\Service\FileSystemStorage')->getIdentity()
        );
    }

}