<?php
namespace Alerts\Service;

use Alerts\Exception\AlertsException;
use Alerts\Model\Alert;
use APIClient\Client;
use IdentityStorage\Components\IdentityInterface;
use Zend\Cache\Storage\Adapter\AbstractAdapter;

/**
 * Class AlertService
 *
 * @package Alerts\Service
 */
class AlertService
{
    /**
     * @var AlertStorage
     */
    private $storage;
    /**
     * @var AbstractAdapter
     */
    private $adapter;
    /**
     * @var IdentityInterface
     */
    private $identity;

    public function __construct(AlertStorage $storage, AbstractAdapter $adapter, IdentityInterface $identity)
    {
        $this->storage  = $storage;
        $this->adapter  = $adapter;
        $this->identity = $identity;
    }

    /**
     * @return int
     */
    public function count()
    {
        return $this->storage->count();
    }

    /**
     * @return AlertStorage
     */
    public function getAll()
    {
        return $this->storage;
    }

    /**
     * @param Alert $alert
     *
     * @return bool
     */
    public function add(Alert $alert)
    {
        if ($this->isDeleted($alert)) {
            return false;
        }

        $this->storage->attach($alert);
        return true;
    }

    /**
     * @param Alert $alert
     */
    public function remove(Alert $alert)
    {
        $this->storage->detach($alert);
    }

    /**
     * @param \stdClass $license
     * @param string    $type
     */
    public function addByLicense($license, $type)
    {
        $item = new Alert();
        $item->setId($license->LicenseID)
            ->setType($type)
            ->setContent($license->ProdNameFull)
            ->setEnabled(1)
            ->setOrder(rand(0, 100));
        $this->add($item);
    }

    /**
     * @param string $hash
     */
    public function deleteAlerts($hash)
    {
        $this->storage->rewind();
        while($this->storage->valid()) {
            if($this->storage->getHash($this->storage->current()) == $hash) {
                $this->storage->detach($this->storage->current());
                break;
            }
            $this->storage->next();
        }

        $this->adapter->setItem($this->identity->veeamId, serialize(array_merge($this->getDeletedIds(), [$hash])));
    }

    public function toArray() {
        $result = [];
        $this->storage->rewind();
        while($this->storage->valid()) {
            $item = (array)$this->storage->current();
            $row = array_combine(array_keys($item), array_values($item));
            $row['hash'] = $this->storage->getHash($this->storage->current());
            $result[] = $row;
            $this->storage->next();
        }

        return $result;
    }

    /**
     * @param Alert $alert
     *
     * @return bool
     */
    protected function isDeleted(Alert $alert)
    {
        $key = $this->storage->getHash($alert);
        $ids = $this->getDeletedIds();
        return in_array($key, $ids);
    }

    /**
     * @return array
     */
    private function getDeletedIds() {
        $ids = $this->adapter->getItem($this->identity->veeamId);
        return $ids === null ? [] : unserialize($ids);
    }
}