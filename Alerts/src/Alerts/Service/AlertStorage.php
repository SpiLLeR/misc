<?php
namespace Alerts\Service;

use Alerts\Model\Alert;

/**
 * Class AlertStorage
 *
 * @package Alerts\Service
 */
class AlertStorage extends \SplObjectStorage
{
    /**
     * @param Alert $object
     *
     * @return string
     */
    public function getHash($object)
    {
        if(!$object instanceof Alert) {
            throw new \InvalidArgumentException('Object must be class Alert.');
        }
        return $object->getType() . '-' . $object->getId();
    }

    /**
     * @return array
     */
    public function toArray() {
        $result = [];
        $this->rewind();
        while($this->valid()) {
            /** @var Alert $item */
            $item = $this->current();
            $row['id'] = $item->getId();
            $row['content'] = $item->getContent();
            $row['type'] = $item->getType();
            $row['image'] = $item->getImage();
            $row['hash'] = $this->getHash($this->current());
            $result[] = $row;
            $this->next();
        }

        return $result;
    }
}